import socket

class webApp:
    def parse(self, received):
        return None

    def process(self, arg):
        http = "200 OK"
        html = "<html><body><h1>Hello World!</h1></body></html>"
        return (http,html)

    def __init__(self, host, port):
        mySocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        mySocket.bind((host, port))
        mySocket.listen(5)

        while True:
            print("Wainting for connections")
            (recvSocket, address) = mySocket.accept()
            print("HTTP request received:")
            received = recvSocket.recv(2048)
            print(received)

            petition = self.parse(received)

            http, html = self.process(petition)

            response = "HTTP/1.1"+http+"\r\n\r\n"+html+"\r\n"
            recvSocket.send(response.encode('uft-8'))
            recvSocket.close()

class contentApp(webApp):
    def parse(self, received):
        return received.decode().split(' ')[1][1:]

    def process(self, arg):
        if arg in self.diccionario:
            http = "200 OK"
            html = "<html><body><h1>"+self.diccionario[arg]+"</h1></body></html>"
        else:
            http = "404 ERROR NOT FOUND"
            html = "<html><body><h1>404 ERROR NOT FOUND</h1></body></html>"
        return http, html


    def __init__(self, host, port):
        self.diccionario = {'/': "Esta es la pagina principal", 'adios': 'Bye Bye'}
        super().__init__(host, port)

if __name__ == "__main__":
    contentApp = contentApp('localhost', 1234)

